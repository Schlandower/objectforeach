# objectforeach

foreach that works with object instead of arrays.
Example:
require ('objectforeach');
var myobj = {"one": "first", "two": "second", "third": "three"};
myobj.for each((value, key) => {
    console.log('Key: ' + key + 'Value: ' + value)
});