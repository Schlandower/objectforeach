'use strict';
(function () {
  if (!Object.prototype.forEach) {
	Object.prototype.forEach = function forEach (callback, thisArg) {
	  if (typeof callback !== 'function') {
		throw new TypeError(callback + ' is not a function');
	  }
	  thisArg = thisArg || this;
	  var keys = Object.keys(thisArg);
	  for(var i = 0, l = keys.length; i !== l; ++i) {
		callback.call(thisArg, this[keys[i]], keys[i], this);
	  }
	};
  }
})();
